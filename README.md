# API_Notificaciones


# Install 
> Use ``bower i https://j0rg3_b43z@bitbucket.org/j0rg3_b43z/api_notificaciones.git`` to install.

# Usage

Include the following code of JavaScript in any view that the user always see, only where the user is Authenticated (is logged in the app).
```javascript
<script>
	function init() {
		var frameNoti = window.frames['frame_not'];
		//The method registerToken has four params all are strings.
		//The first param is the Name of the App that You're using
		//The second param is the ID of the user in session that soulve is linked with the token 
		//The third param is the Name of the user in session that soulve is linked with the token and ID
		//The fourth param is the Group of the user in session that soulve is linked with the token, ID and Name
		frameNoti.registerToken('appName','User ID','User name','Group');
	}
	function showNotification(payload){
		//Handle how show notification in payload param.
		//payload.notification.title <- Get the Title of Notification.
		//payload.notification.body <- Get the body of Notification.
		//payload.notification.icon <- Get the icon of Notification.
	}
	function deleteToken(){
			var frameNoti = window.frames['frame_not'];
			frameNoti.deleteFirebaseService('appName','User ID');
		}
<script/>
```

Include the following HTML

```HMTL
<iframe onload="init()" name="frame_not" src="./bower_components/api_notificaciones/index.html" style="boder:0;display: none;"></iframe>
```
# Logout

When the user click on the "log out" (when the cookie or session variables are ended)
call the following method of JavaScript to destroy the notifications token.

```javascript
deleteToken();
```
# Send One Notification

For send a notification, we make a request to server, this code should be implemented in the Back-End, something like this.

I create a file named sendNotification.php
```php
	function sendNotificacion($app, $id, $title, $body){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://oxoo.000webhostapp.com/notificaciones/sendOneSMS?app=$app&id=$id&title=$title&body=$body&icon=https://localhost/basica-web/assets/images/logo.png",
		  CURLOPT_CUSTOMREQUEST => "GET"
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		  return FALSE;
		} else {
		  echo $response;
		  return TRUE;
		}	
	}
```

and for include in the place that you want to use, use something like this

```php
require_once 'sendNotificaciones.php';
...

	if(sendNotificacion('appName','user for send notification','title','body')){
			// all is correct, the notification was send
		}
...
```

#Usage in ionic

install `ionic cordova plugin add cordova-plugin-firebase`

review your `package.json` and view the version of @ionic-native/core and put the same version in the place 'X.X.X'

install `npm install --save @ionic-native/firebase@X.X.X`

download the files: 
* GoogleService-Info.plist 	=> for IOS
* google-services.json 		=> for Android
Use the same ID in the register in firebase and `config.xml` ID property of widget component

Now in `app.module.ts` put this.
```javascript
//Notificaciones
import { AngularFireModule } from '@angular/fire';
import { Firebase } from '@ionic-native/firebase/ngx';
import { FcmService } from './services/fcm.service'

const config = {
  apiKey: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
  authDomain: "xxxxxxxxxxxxxxxxxxxxx.firebaseapp.com",
  databaseURL: "https://xxxxxxxxxx.firebaseio.com",
  projectId: "xxxxxxxxxxxxxxxxxxxxxx",
  storageBucket: "xxxxxxxxxxxxxxxxxx.appspot.com",
  messagingSenderId: "xxxxxxxxxxxxxxxxxx"
};

...
imports: [AngularFireModule.initializeApp(config)],
providers: [Firebase,FcmService,]
```
Create the service FCM:
```javascript
import { Injectable } from '@angular/core';
import { Firebase } from '@ionic-native/firebase/ngx';
import { Platform } from '@ionic/angular';
import { InicioHTTP } from '../services/http.service';

@Injectable()
export class FcmService {

  constructor(private firebase: Firebase,
              private platform: Platform,
              private inicioHttpService: InicioHTTP) {}

  async getToken() {
    let token;

    if (this.platform.is('android')) {
      token = await this.firebase.getToken();
    }

    if (this.platform.is('ios')) {
      token = await this.firebase.getToken();
      await this.firebase.grantPermission();
    }

    this.saveToken(token);
  }

  private saveToken(token) {
  	// use this for save token with this API 
    //this.inicioHttpService.saveToken(token).subscribe((resp)=>{
	//	
    //});
  }

  onNotifications() {
    return this.firebase.onNotificationOpen();
  }
}
```

And implement, some like this:
```javascript
//notificaciones
import { FcmService } from '../services/fcm.service';
import { Platform } from '@ionic/angular';

constructor(
    private platform: Platform,
    private fcmService: FcmService) {
		// NOTIFICACIONES
        this.fcmService.getToken();
        this.fcmService.onNotifications().subscribe((msg)=>{
          if (this.platform.is('ios')) {
            console.log(msg)
            // this.presentToast(msg.aps.alert);
          } else {
            console.log(msg)
            // this.presentToast(msg.body);
          }
        });
	}
```
