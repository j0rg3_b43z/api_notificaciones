// Initialize Firebase
var app;var ID;var nombre;var group;
	function registerToken(app, ID, nombre, group){
		this.app = app;
		this.ID = ID;
		this.nombre = nombre;
		this.group = group;
	}

	var config = {
		apiKey: "AIzaSyB1x04uFoz8TBm0QM2_8EJNKmxWrr1srh8",
		messagingSenderId: "218563715093"
	};

	firebase.initializeApp(config);
	navigator.serviceWorker.register('./firebase-messaging-sw.js')
	.then((registration) => {
	  messaging.useServiceWorker(registration);
	  messaging.getToken().then((currentToken)=> {
		if (currentToken) {
			console.log('getToken: '+currentToken);
			if(localStorage.getItem('token') != currentToken || localStorage.getItem('token') == ''){
				sendTokenToServer(currentToken);
			}
			localStorage.setItem('token',currentToken);
	  	} else {
	    	// Show permission request.
	    	console.log('No Instance ID token available. Request permission to generate one.');
	    	// Show permission UI.
	    	messaging.requestPermission()
			.then(()=>{
				console.log('I have permission');
				return messaging.getToken();
			})
			.then((token)=>{
		    	console.log(token);
		    	sendTokenToServer(token)
		    	localStorage.setItem('token',token);
		    })
			.catch((err)=>{
				console.log('error'+err);
			});
	  	}
	}).catch(function(err) {
	  	console.log('An error occurred while retrieving token. ', err);
	  	document.body.innerText = err;
	});
	});

	const messaging = firebase.messaging();
	const http = new XMLHttpRequest();
	// const url = 'http://localhost/Back-End-notificaciones/index.php';
	const url = 'https://oxoo.000webhostapp.com/notificaciones/index.php';

	messaging.onTokenRefresh(function() {
		messaging.getToken().then(function(refreshedToken) {
			console.log('Token refreshed.');
			sendTokenToServer(refreshedToken);
			localStorage.setItem('token',refreshedToken);
		}).catch(function(err) {
	    	console.log('Unable to retrieve refreshed token ', err);
	    	showToken('Unable to retrieve refreshed token ', err);
	  	});
	});

	messaging.onMessage((payload)=>{
		try{
			window.parent.showNotification(payload);
		}catch(e){
			console.log("Error, showNotification is not defined"+e);
		}
	});

	function sendTokenToServer(token){
		http.open('POST',url);
		http.onreadystatechange = function () {
	        if(http.readyState === XMLHttpRequest.DONE && http.status === 200) {
	            console.log(http.responseText);
	        }
	        if(http.readyState === XMLHttpRequest.DONE && (http.status === 500 || http.status === 404)){
	        	localStorage.setItem('token','');
	        }
	   	};
		http.send(JSON.stringify({"app"	:app,"id":ID,"name":nombre,"grupo":group,"token":token}));
		console.log('App: '+app+" id: "+ID+ " name: "+nombre+" grupo:"+group);
	}

	function deleteTokenToServer(app,id,token){
		http.open('POST',url+"/deleteToken");
		http.onreadystatechange = function () {
	        if(http.readyState === XMLHttpRequest.DONE && http.status === 200) {
	            console.log(http.responseText);
	        }
	   	};
		http.send(JSON.stringify({"app"	:app,"id":ID,"token":token}));
	}

	function deleteFirebaseService(app,id){
		var token = localStorage.getItem('token');
		messaging.deleteToken(token).then((success)=>{
			deleteTokenToServer(app,id,token);
			console.log(success)
		});
		localStorage.setItem('token','')
	}